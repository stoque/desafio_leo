<?php session_start();
	spl_autoload_register('autoloader');

	function autoloader($class){
		include("$class.php");
	}

	$db = new Db('my_sqlite_db.db');
?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>LEO Learning</title>
	<link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="assets/styles/vendor/owl.carousel.min.css">
	<link rel="stylesheet" href="assets/styles/main.css">
</head>
<body>
	
	<header class="main-header">
		<div class="container">
			<h1 class="logo">
				<a href="/">
					<img src="assets/images/logo.svg" alt="Logo LEO">
				</a>
			</h1>

			<form class="search-form">
				<input type="search" class="input" placeholder="Pesquisar cursos...">
				<button class="button">
					<svg class="icon">
						<use xlink:href="assets/images/icons.svg#icon-search"></use>
					</svg>
				</button>
			</form>

			<div class="account-wrapper">
				<img class="avatar" src="https://avatars3.githubusercontent.com/u/12236827?v=4&s=460" alt="Foto de avatar">

				<div class="text-wrapper">
					<p class="message">Seja bem-vindo</p>
					<strong class="name">Lucas Stoque</strong>
					
				</div>

				<span class="arrow"></span>
			</div>
		</div>
	</header>	
	
	<main>
		<div class="owl-carousel">
			<?php 
				$sliders = $db->getAllSlides();
				while($slide = $sliders->fetchArray()):
			?>
				<div class="banner-wrapper">
					<img class="image" src="<?php echo $slide['imagem'] ?>"></img>

					<div class="infos-wrapper">
						<div class="container">
							<div class="info-box">
								<p class="title"><?php echo $slide['titulo'] ?></p>
								<p class="description"><?php echo $slide['descricao'] ?></p>
								<a class="action" href="<?php echo $slide['link'] ?>">Ver curso</a>
							</div>
						</div>
					</div>

				</div>
			<?php endwhile; ?>
		</div>

		<section class="my-courses">
			<h2 class="title">Meus Cursos</h2>

			<ul class="courses-list">
				<?php
					$courses = $db->getAllCourses();
					while ($course = $courses->fetchArray()):
				?>
					<li class="course">
						<img src="<?php echo $course['imagem'] ?>" alt="Imagem ilustrativa do curso" class="image">

						<div class="infos">
							<h3 class="name"><?php echo $course['nome'] ?></h3>
							<p class="text"><?php echo $course['resumo'] ?></p>
						
							<a href="<?php echo $course['link'] ?>" class="action">Ver Curso</a>
						</div>
					</li>
				<?php endwhile; ?>

				<li class="course -upload">
					<svg class="icon">
						<use xlink:href="./assets/images/icons.svg#icon-folder-upload"></use>
					</svg>

					<p class="text">Adicionar <br> curso</p>
				</li>									
			</ul>
		</section>
	</main>
	
	<footer class="main-footer">
		<div class="top">
			<div class="top-inner">
				<div class="logo">
					<a href="/">
						<img src="assets/images/logo-footer.svg" alt="Logo LEO">
					</a>
					<p>Lorem ipsum dolor sit amet, consectetur.</p>
				</div>

				<div class="contact">
					<p><strong>// Contato</strong></p>
					<p>(21) 98765-3434</p>
					<a href="mailto:contato@leolearning.com">contato@leolearning.com</a>
				</div>

				<div class="social">
					<p><strong>// Social</strong></p>

					<ul class="list">
						<li class="item">
							<a href="#">
								<svg class="icon">
									<use xlink:href="./assets/images/icons.svg#icon-social-facebook"></use>
								</svg>
							</a>
						</li>
						<li class="item">
							<a href="#">
								<svg class="icon">
									<use xlink:href="./assets/images/icons.svg#icon-social-pinterest"></use>
								</svg>
							</a>
						</li>
						<li class="item">
							<a href="#">
								<svg class="icon">
									<use xlink:href="./assets/images/icons.svg#icon-social-twitter"></use>
								</svg>
							</a>
						</li>
					</ul>
				</div>
			</div>
		</div>

		<div class="bottom">
			<div class="bottom-inner">
				<p>Copryight 2017 - All right reserved.</p>
			</div>
		</div>
	</footer>

	<div class="modal" data-js="modal">
		<div class="overlay" data-js="modal-overlay"></div>
		<div class="content" data-js="modal-content">
			<button class="close" data-js="modal-close">
				<svg class="icon">
					<use xlink:href="./assets/images/icons.svg#icon-close"></use>
				</svg>
			</button>

			<img class="image" src="./assets/images/modal-image.png" alt="Imagem do modal">

			<h3 class="title">EGESTAS TORTOR VULPUTATE</h3>

			<p class="text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsa saepe hic similique sapiente voluptates, laborum atque nulla.</p>

			<a class="action" href="#">Inscreva-se</a>
		</div>
	</div>

	<script src="./assets/scripts/vendor/jquery.min.js"></script>
	<script src="./assets/scripts/vendor/owl.carousel.min.js"></script>
	<script src="./assets/scripts/main.js"></script>
</body>
</html>