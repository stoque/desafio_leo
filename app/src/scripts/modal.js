(function(window, document) {
  const modal = document.querySelector('[data-js="modal"]');
  const overlay = document.querySelector('[data-js="modal-overlay"]');
  const content = document.querySelector('[data-js="modal-content"]');
  const close = document.querySelector('[data-js="modal-close"]');

  if (!localStorage.getItem('modal')) {
    modal.classList.toggle('is-active');
  }

  localStorage.setItem('modal', 1);

  close.addEventListener('click', closeModal);
  overlay.addEventListener('click', closeModal);

  function closeModal() {
    content.setAttribute('style', 'transform: translateY(0)');
    modal.classList.toggle('is-active');
  }

})(window, document);