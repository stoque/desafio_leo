<?php
  class Db {
    public $db;

    public function __construct($db) {
      $this->db = new SQLite3($db);
      $this->init();
    }

    private function init() {
      $this->createCoursesTable();
      $this->createSlidesTable();
    }

    public function createCoursesTable() {
      return $this->db->exec('CREATE TABLE IF NOT EXISTS cursos (nome STRING, resumo STRING, link STRING, imagem STRING)');
    }

    public function createSlidesTable() {
      return $this->db->exec("CREATE TABLE IF NOT EXISTS slides (imagem STRING, titulo STRING, descricao STRING, link STRING)");
    }

    public function dropCoursesTable() {
      return $this->db->exec("DROP TABLE cursos");
    }

    public function dropSlidesTable() {
      return $this->db->exec("DROP TABLE slides");
    }

    public function insertCourse($nome, $resumo, $link, $imagem) {
      return $this->db->exec("INSERT INTO cursos (nome, resumo, link, imagem) VALUES ('$nome', '$resumo', '$link','$imagem')");
    }

    public function insertSlide($imagem, $titulo, $descricao, $link) {
      return $this->db->exec("INSERT INTO slides (imagem, titulo, descricao, link) VALUES ('$imagem', '$titulo', '$descricao', '$link')");
    }

    public function getAllSlides() {
      return $this->db->query("SELECT rowid, * FROM slides");
    }

    public function getAllCourses() {
      return $this->db->query("SELECT rowid, * FROM cursos");
    }

    public function deleteCourse($id) {
      return $this->db->query("DELETE FROM cursos WHERE rowid = $id");
    }

    public function deleteSlide($id) {
      return $this->db->query("DELETE FROM slides WHERE rowid = $id");
    }
  }
?>