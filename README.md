# Desafio LEO

### Instalação

Para rodar esse app é necessário ter o Node e o PHP (com SQLite3 ativado) instalados na sua maquina.

```
# Clone esse repositório
$ git clone git@bitbucket.org:stoque/desafio_leo.git

# Installe o gulp globalmente
$ sudo npm install -g gulp

# Entre na pasta do projeto
$ cd desafio_leo

# Installe todas as depêndencias do projeto
$ npm install

# Subir um ambiente de dev local: http://localhost:3000
$ npm run dev

# Subir um ambiente de produção local: http://localhost:8888
$ npm run prod
```

