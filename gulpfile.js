// =========================
// Modules
// =========================
const gulp 					= require('gulp'),
			sass 					= require('gulp-sass'),
			cssnano 			= require('cssnano'),
			autoprefixer	= require('autoprefixer'),
			postcss				= require('gulp-postcss'),
			lost					= require('lost'),
			sourcemaps		= require('gulp-sourcemaps'),
			cssmin				= require('gulp-cssmin'),
			plumber				= require('gulp-plumber'),
			uglify 				= require('gulp-uglify'),
			concat				= require('gulp-concat'),
			gcmq 					= require('gulp-group-css-media-queries'),
			rename				= require('gulp-rename'),
			browserSync 	= require('browser-sync');

// Directories
const path = {
	dev		: 'app/src',
	prod	: 'app/assets'
}

// =========================
// Tasks
// =========================

// Call Browser-Sync
gulp.task('browser-sync', () => {
	browserSync.init(['app/assets/styles/*.css', 'app/assets/scripts/*.js', 'app/*.php'], {
		notify: {
			styles: {
				top: 'auto',
				bottom: '0'
			}
		},
		proxy: 'localhost:8888'
	})
});

// Call Sass
gulp.task('sass', () => {
	const processors = [
		autoprefixer, lost
	]

	gulp.src( path.dev + '/sass/main.scss')
		.pipe(plumber())
		.pipe(sourcemaps.init())
		.pipe(sass().on('error', sass.logError))
		.pipe(sourcemaps.write()) 
		.pipe(gcmq())
		.pipe(postcss(processors))
		.pipe(gulp.dest(path.prod + '/styles'));
});

// Call CSS minify
gulp.task('css-min', () => {
	gulp.src( path.prod + '/styles/main.css')
		.pipe(cssmin())
		.pipe(gulp.dest(path.prod + '/styles'));
});

// Call javascript uglify and concat
gulp.task('js', () => {
	return gulp.src(path.dev + '/scripts/**/*.js')
		.pipe(plumber())
		.pipe(concat('main.js'))
		.pipe(uglify())
		.pipe(gulp.dest(path.prod + '/scripts'))
});

// Call Watch
gulp.task('watch', ['sass', 'browser-sync'], () => {
	gulp.watch( path.dev + '/sass/**/**.scss', ['sass']);
	gulp.watch( path.dev + '/scripts/**/*.js', ['js']);
	gulp.watch('./app/*.html');
});

gulp.task('dev', ['js','sass', 'browser-sync', 'watch']);
gulp.task('prod', ['js','sass', 'css-min', 'browser-sync', 'watch']);
